import React from "react"
import { makeStyles } from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import {Rating} from "@material-ui/lab";
import ReadMoreReact from 'read-more-react';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3),
    },
    paper: {
        margin: `${theme.spacing(1)}px auto`,
        padding: theme.spacing(2),
    },
    commentText: {
        fontSize: 14,
        textAlign: 'left'
    },
    subText: {
        fontSize: 10,
        textAlign: 'left',
        color: 'gray'
    },
    rating: {
        textAlign:'left',
    },
    shopLogo: {
        textAlign: 'center',
    },
    dateText: {
        fontSize: 9,
        color: "gray",
        textAlign:'left'
    }

}));


export default function CommentBox() {
    const classes = useStyles();
    return (
        <div className={classes.root}>

            <Paper className={classes.paper} elevation={3}>
                <Grid container wrap="nowrap" spacing={3} direction="column">
                    <Grid item xs={12}>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            <Grid xs={1}>
                                <Avatar src="https://kurumsal.n11.com/assets/logo/logo-n11-usshape2-large.png"/>
                            </Grid>
                            <Grid>
                                <Rating value={3} precision={0.5} max={5} readOnly size="medium"/>
                            </Grid>
                            <Grid>
                                <Typography className={classes.dateText}>29.05.2020</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container direction="column" justify="space-between">

                            <Grid>
                                <Typography className={classes.commentText}>
                                    <ReadMoreReact
                                        min={50}
                                        ideal={150}
                                        max={200}
                                        readMoreText={"Devamını oku.."}
                                        text={"telefon telefon değil resmen yürüyen teknoloji içind eher şey var donma kasma sorunu yok mükemmel fotğraf deneyimi sunuyor harika geniş güzel kailteli ekranı var mükemmell biraz pahalı ama buna deycek bir para oyun deneyii harika 2 günde akpıma geldi hızlı ve güvenilir adres güvenilir alışveriş, hızlı ve güven veren kargo sayesinde mutlu yüzler"} />
                                </Typography>
                            </Grid>
                            {/*<Grid item>
                                <Typography className={classes.subText}>Gittigidiyor.com kullanıcıs yorumu</Typography>
                            </Grid>*/}
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>


    )
}