import React from "react"
import {Box, Typography} from "@material-ui/core"
import {Rating} from '@material-ui/lab'
import StarBorderIcon from '@material-ui/icons/StarBorder'
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    commentCount: {
        fontSize: 12,
        fontColor: 'gray',
        paddingLeft: 20
    }
}))

export default function Rate() {
    const classes = useStyles();
    return (
        <Grid container direction="row" >
            <Box component="fieldset" mb={0} borderColor="transparent">
                <Rating
                    name="customized-empty"
                    defaultValue={2.5}
                    precision={0.1 }
                    emptyIcon={<StarBorderIcon fontSize="inherit" />}
                />
            </Box>
            <div className={classes.commentCount}> (803 oy) </div>
        </Grid>
    )
}
