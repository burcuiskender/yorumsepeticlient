import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Skeleton from '@material-ui/lab/Skeleton';
import Rate from "./Rating";
import Comments from "./Comments";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from "@material-ui/core/Chip";
import Divider from "@material-ui/core/Divider";


const useStyles = makeStyles((theme) => ({
    card: {
        maxWidth: '100%',
        margin: theme.spacing(2),
        height: '200',
        textAlign: 'left'
    },
    column: {
        flexBasis: '33.33%',
        textAlign: 'left'
    },
    details: {
        display: 'block'
    },
    summary: {
    },
    avatar: {
        height: 130,
        width: 130,
        marginRight: 20
    },
    productName: {
        paddingLeft: 20,
        textAlign: 'left'
    }
}));

export default function Product() {
    const classes = useStyles();

    return (
        <ExpansionPanel className={classes.summary}>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1c-content"
                id="panel1c-header"
                className={classes.summary}
            >
                <Avatar
                    variant="square"
                    className={classes.avatar}
                    src='https://images-na.ssl-images-amazon.com/images/I/81MlgsZU0pL._AC_SL1500_.jpg'
                />

                <div>
                    <Typography className={classes.productName} >Philips BHH880/00 StyleCare Essential Isıtmalı Düzleştirme Fırcası</Typography>
                    <div className={classes.column}>
                        <Rate/>
                    </div>
                </div>
            </ExpansionPanelSummary>
            <Divider />
            <ExpansionPanelDetails className={classes.details}>
                <Typography variant="body2" color="textSecondary" component="p">hangi sitelerde var</Typography>
                <Typography variant="body2" color="textSecondary" component="p">hangi sitelerde var</Typography>
                <Typography variant="body2" color="textSecondary" component="p">hangi sitelerde var</Typography>
            </ExpansionPanelDetails>
        </ExpansionPanel>

    );
}

