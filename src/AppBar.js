import React from 'react';
import {Toolbar, AppBar, IconButton, Typography, InputBase} from '@material-ui/core';
import { fade, makeStyles } from '@material-ui/core/styles';


    const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#29434e'
    },
    title: {
        flexGrow: 1,
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '30ch',
            '&:focus': {
                width: '38ch',
            },
        },
    },
}));

    export default function SearchAppBar() {
    const classes = useStyles();

    return (

            <AppBar position="fixed" className={classes.root}>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Hangi Ürün?
                    </Typography>
                    <div className={classes.search}>
                        <InputBase
                            placeholder="Ürün ara…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Toolbar>
            </AppBar>
    );
}