import React from "react"
import { makeStyles } from '@material-ui/core/styles';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import * as PropTypes from "prop-types";

import CommentBox from "./CommentBox";


const useStyles = makeStyles((theme) => ({
    content: {
        padding: theme.spacing(0),
        textAlign: 'center',
        width: '100%',
    },
    tabs: {
        display: 'block',
        width: '100%',
    },
    tab: {
        width: '50%'
    },

    card: {
        position: 'relative',
    },
    commentBox: {

    }

}));

export default function Comments() {
    const classes = useStyles()

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Card className={classes.card}>
            <CardContent>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                    className={classes.tabs}
                >
                    <Tab label="NEDEN İYİ"  {...a11yProps(0)} className={classes.tab} />
                    <Tab label="NEDEN KÖTÜ"  {...a11yProps(1)} className={classes.tab}/>
                </Tabs>
            </CardContent>
            <CardContent >
                <TabPanel value={value} index={0}>
                    <CommentBox className={classes.commentBox}/>
                    <CommentBox/>
                    <CommentBox/>
                    <CommentBox/>
                 </TabPanel>
                <TabPanel value={value} index={1}>
                    Kötü Yorumlar
                </TabPanel>
            </CardContent>
        </Card>

     );
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (children)}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
}