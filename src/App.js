import React from 'react';
import './App.css';

import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import AppBar from "./AppBar";
import Product from "./Product";
import Comments from "./Comments";

const useStyles = makeStyles({
    container: {
        position: 'relative',
        paddingTop: 80,
        backgroundColor: '#e0e0e0'
    }
});

function App() {
    const classes = useStyles();

    return (
        <div className="App">
            <AppBar/>
            <Container className={classes.container}>
                <Product/>
                <Comments/>
            </Container>
        </div>
    );
}

export default App;
